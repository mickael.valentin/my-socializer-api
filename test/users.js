const mongoose = require('mongoose');
const User = require('../src/models/user.model');
const chai = require('chai');
const chaihttp = require('chai-http');
var faker = require('faker');
let userId;

chai.use(chaihttp);
const should = chai.should();

var randomEmail = faker.internet.email(); 
var randomPassword = faker.internet.password();
var randomFirstName = faker.name.firstName(); 
var randomLastName = faker.name.lastName(); 
var randomBoolean = faker.random.boolean();

describe('CRUD users', () => {
    describe('Create user', () =>{
        it('It should create a user', (done) => {
            chai.request('localhost:3000/api')
            .post('/users')
            .send({
                "email":randomEmail,
                "password":"password1234",
                "firstname":randomFirstName,
                "lastname":randomLastName,
                "admin":randomBoolean,
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                userId = res.body._id;
                done()
            })
        })
    });
    describe('Get users', () =>{
        it('It should get all users', (done) => {
            chai.request('localhost:3000/api')
            .get('/users')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done()
            })
        })
    });
    describe('Get one user', () =>{
        it('It should get one user', (done) => {
            chai.request('localhost:3000/api')
            .get('/users/${userId}')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done()
            })
        })
    });
    describe('Update user', () =>{
        it('It should update a user', (done) => {
            chai.request('localhost:3000/api')
            .put('/users/${userId}')
            .send({
                "email":randomEmail,
                "password":"password1234",
                "firstname":randomFirstName,
                "lastname":randomLastName,
                "admin":randomBoolean,
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done()
            })
        })
    });
    describe('Delete one user', () =>{
        it('It should delete one user', (done) => {
            chai.request('localhost:3000/api')
            .delete('/users/${userId}')
            .end((err, res) => {
                res.should.have.status(200);
                done()
            })
        })
    });
    describe('Delete all users', () =>{
        it('It should delete all users', (done) => {
            chai.request('localhost:3000/api')
            .delete('/users')
            .end((err, res) => {
                res.should.have.status(200);
                done()
            })
        })
    });
})