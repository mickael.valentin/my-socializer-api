const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userValidator = require('../utilities/validators/user.validator');

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        minlength: 4,
        maxlength: 128
    },
    firstname: {
        type: String,
        maxlength: 50
    },
    lastname: {
        type: String,
        maxlength: 50
    },
    username: {
        type: String,
        maxlength: 50
    },
    address: {
        type: String,
        maxlength: 50
    },
    number: {
        type: String,
        maxlength: 10
    },
    /*admin: {
        type: Boolean
    }*/
}, {
    timestamps: true
});

/*userSchema.methods.joiValidate = (obj) => {
    let Joi = require('joi');
    let schema = userValidator.create;
    return Joi.validate(obj, schema);
}*/


module.exports = mongoose.model('User', userSchema)
