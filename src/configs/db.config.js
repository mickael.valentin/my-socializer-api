module.exports = {
    mongo: {
        env:process.env.APP_ENV,
        url:process.env.DB_URL,
        urltest:process.env.DB_TEST_URL
    }
}