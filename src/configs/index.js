const dbConfig = require('./db.config');
const jwtConfig = require('./jwt.config');
const ServerConfig = require('./server.config');

exports.database = dbConfig;
exports.jwt = jwtConfig;
exports.server = ServerConfig;

