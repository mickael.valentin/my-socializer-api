const mongoose = require('mongoose');

exports.connect = (url, urltest, env) => {
    if(env == "dev"){
        mongoose.connect(url, { useNewUrlParser: true , useCreateIndex: true}).then(() => {
            console.log("Successfully connected to the dev database");
        }).catch(err => {
            console.log('Could not connect to the dev database.', err);
            process.exit();
        });
    } else if(env == "test"){
        mongoose.connect(urltest, { useNewUrlParser: true , useCreateIndex: true}).then(() => {
            console.log("Successfully connected to the test database");
        }).catch(err => {
            console.log('Could not connect to the test database.', err);
            process.exit();
        });
    }
}
