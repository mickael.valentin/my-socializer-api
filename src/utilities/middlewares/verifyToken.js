const jwt = require('jsonwebtoken');
const config = require('../../configs');

function verifyToken(req, res, next){
    let token = req.headers['x-access-token'];
    if(!token){
        return res.status(400).send({auth:false,message:"No token provided"});
    }
    jwt.verify(token, config.jwt.jwt.secret, function(err, decoded){
        if(err){
            return res.status(401).send({auth:false,message:'no authorized'})
        }
        req.user = decoded;
        next()
    })
}

module.exports = verifyToken;