const User = require('../models/user.model.js');
const Joi = require('joi');
const bcrypt = require('bcrypt');

// Create and Save a new user
exports.create = (req, res) => {

    const user = new User({
        email: req.body.email,
        password: req.body.password,
        firstname:req.body.firstname,
        lastname:req.body.lastname,
        admin:req.body.admin
    });

    let arrayErr = user.joiValidate(req.body);
    if(arrayErr.error) {
        res.send(arrayErr);
    }
    else {
        let hashedpassword = bcrypt.hashSync(req.body.password, 10);
        user.password = hashedpassword;
        user.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the user."
            });
        });
    }
};

// Find and return all User from the database.
exports.findAll = (req, res) => {

    User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding users."
        });
    });
};

// Find a single user with id
exports.findOne = (req, res) => {
    User.findById(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params.id
        });
    });
};

// Update a user
exports.update = (req, res) => {

    User.findByIdAndUpdate(req.params.id, {
        email: req.body.email,
        firstname:req.body.firstname,
        lastname:req.body.lastname,
        address:req.body.address,
        number:req.body.number,
    }, {new: true})
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'id') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Error updating user with id " + req.params.id
        });
    });

};

// Update an user password 
exports.updatePassword = (req, res) => {

    User.findByIdAndUpdate(req.params.id, {
        password: req.body.password,
    }, {new: true})
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'id') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Error updating user password with id " + req.params.id
        });
    });

};

// Delete a user with the specified userid in the request
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });
        }
        res.send({message: "User deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Could not delete user with id " + req.params.id
        });
    });

};

exports.deleteAll = (req, res) => {
        User.deleteMany()
            .then(() => {
                res.status(204);
                res.end();
            })
            .catch((err) => {
                res.status(500);
                res.send({
                    'code': res.statusCode,
                    'error': err.message
                });
            });

}
