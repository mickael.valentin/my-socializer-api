const jwt = require('jsonwebtoken');
const User = require('../models/user.model.js');

exports.register = (req, res) => {


    User.create({
        email : req.body.email,
        password : req.body.password,
        firstname:req.body.firstname,
        lastname:req.body.lastname,
        username:req.body.username
        //admin:req.body.role
    },
    function (err, user) {

        if (err) return res.status(500).send(err);

        // create a token
        let token = jwt.sign({ id: user._id }, "supersecret", {
            expiresIn: 86400
        });

        res.status(200).send({ auth: true, token: token, body: req.body });

    });

}

exports.sign = (req, res, next) => {

    var token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

    jwt.verify(token, "supersecret", function(err, decoded) {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

        User.findById(decoded.id, { password: 0 }, function (err, user) {
            if (err) return res.status(500).send("There was a problem finding the user.");
            if (!user) return res.status(404).send("No user found.");

            next(user);
        });
    });

}

exports.login = (req, res) => {

    User.findOne({ email: req.body.email }, function (err, user) {

        if (err) return res.status(500).send('Error on the server.');

        if (!user) return res.status(404).send('No user found.');

        let token = jwt.sign({ id: user._id }, "supersecret", {
            expiresIn: 86400
        });

        res.status(200).send({ id: user._id, auth: true, token: token });
    });

};

exports.logout = (req, res) => {
    res.status(200).send({ auth: false, token: null });
}
