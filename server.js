require('dotenv').config();
const config = require('./src/configs');
const MongoService = require('./src/services/mongoose.service');
const ServerService = require('./src/services/server.service');

MongoService.connect(config.database.mongo.url, config.database.mongo.urltest, config.database.mongo.env);

ServerService.start(config.server.port);


